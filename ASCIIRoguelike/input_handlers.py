from typing import Optional
import tcod.event
from actions import Action, EscapeAction, MovementAction


class EventHandler(tcod.event.EventDispatch[Action]):
    def ev_quit(self, event: "tcod.event.Quit") -> Optional[Action]:
        raise SystemExit()

    def ev_keydown(self, event: "tcod.event.KeyDown") -> Optional[Action]:
        action: Optional[Action] = None

        key = event.sym

        # move up
        if key == tcod.event.K_k:
            action = MovementAction(dx=0, dy=-1)
        # move down
        elif key == tcod.event.K_j:
            action = MovementAction(dx=0, dy=1)
        # move left
        elif key == tcod.event.K_h:
            action = MovementAction(dx=-1, dy=0)
        # move right
        elif key == tcod.event.K_l:
            action = MovementAction(dx=1, dy=0)
        # quit
        elif key == tcod.event.K_ESCAPE:
            action = EscapeAction()

        # no valid key was pressed
        return action
