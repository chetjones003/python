#!/usr/bin/env python3
import tcod
from actions import EscapeAction, MovementAction
from entity import Entity
from input_handlers import EventHandler


def main() -> None:
    # hard code width/height (change later)
    screen_width = 80
    screen_height = 50

    # grabbing ascii tileset from file
    tileset = tcod.tileset.load_tilesheet(
        # path, columns, rows, charmap::iterable
        "./data/char.png", 32, 8, tcod.tileset.CHARMAP_TCOD
    )

    # instance event_handler
    event_handler = EventHandler()

    # Initialize Entities
    player = Entity(int(screen_width / 2), int(screen_height / 2), "@", (255, 255, 255))
    npc = Entity(int(screen_width / 2 - 5), int(screen_height / 2), "N", (255, 255, 0))
    entities = {npc, player}

    # defining new terminal values
    with tcod.context.new_terminal(
            screen_width,
            screen_height,
            tileset=tileset,
            title="ASCII Roguelike",
            vsync=True,
    ) as context:
        # order F tells numpy to access 2D array as [x][y]
        root_console = tcod.Console(screen_width, screen_height, order="F")
        # game loop
        while True:
            # print player at 1,1
            root_console.print(x=player.x, y=player.y, string=player.char, fg=player.color)

            # update screen
            context.present(root_console)

            # refresh screen
            root_console.clear()

            # wait for any event
            for event in tcod.event.wait():
                action = event_handler.dispatch(event)

                # if no action is pressed move on
                if action is None:
                    continue

                # if action is instance of movement then move player
                if isinstance(action, MovementAction):
                    player.move(dx=action.dx, dy=action.dy)

                # if esc key is pressed quit
                elif isinstance(action, EscapeAction):
                    raise SystemExit()


if __name__ == "__main__":
    main()
